import * as DiceHelper from "./Dice";

export class Craps {
  current_point: number;
  dice1: DiceHelper.Dice;
  dice2: DiceHelper.Dice;
  offset: number = 1;

  constructor() {
    this.dice1 = new DiceHelper.Dice();
    this.dice2 = new DiceHelper.Dice();
    this.current_point = null;
  }
  roll(): string {
    let value: number = this.dice1.roll() + this.dice2.roll();

    if (this.current_point == null) {
      if (value === 7 || value === 11) {
        // winner
        this.offset = 1;
        return value + "! You win!";
      }

      if ([2, 3, 12].includes(value)) {
        this.offset = -1;
        if (this.dice1.value === 1 && this.dice2.value === 1) {
          return "Snake Eyes.  Craps, you lose.";
        }
        return value + ".  Craps, you lose.";
      }
      this.current_point = value;
      this.offset = 1;
      return ("A " + this.dice1.value + " and a " + this.dice2.value + ". Point set at " + this.current_point);
    } else {
      if (value === 7) {
        this.current_point = null;
        this.offset = -1;
        // lose
        return value + ".  Craps, you lose.";
      }

      if (value === this.current_point) {
        this.offset = 1;
        if (this.current_point === 4 || this.current_point === 10) {
          this.offset = 2;
        } else if (this.current_point === 6 || this.current_point === 8) {
          this.offset = 1.2;
        } else if (this.current_point === 5 || this.current_point === 9) {
          this.offset = 1.5;
        }
        this.current_point = null;
        return value + "! Point Made. You win!";
      }
    }
    return value + ".  Roll again.";
  }
}
