export class Dice
{
  sides: number;
  value: number;
  constructor(sides: number = 6) 
  {
  this.sides = sides;
  }

  roll():number {
    this.value = Math.floor(Math.random() * this.sides) + 1;
    return this.value;
  }
}